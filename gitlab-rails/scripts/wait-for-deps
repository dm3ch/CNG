#!/bin/bash

WAIT_FOR_TIMEOUT="${WAIT_FOR_TIMEOUT:-5}"

# Configure for which schema to be verifying
SCHEMA_FILE=db/structure.sql
SCHEMA_RAKE_TASK=db:version
SCHEMA_FORMAT=sql

cd /srv/gitlab

# Retain support for older schema if needed
if [ ! -f "db/structure.sql" ]; then
  SCHEMA_FILE=db/schema.rb
  SCHEMA_FORMAT=ruby
fi

if [ "${DB_SCHEMA_TARGET,,}" == "geo" ]; then
  SCHEMA_FILE=ee/db/geo/schema.rb
  SCHEMA_RAKE_TASK=geo:db:version
  SCHEMA_FORMAT=ruby
fi

# fetch the schema desired version directly from the source code
if [ "${SCHEMA_FORMAT,,}" == "ruby" ]; then
  SCHEMA_VERSION=$(grep '(version: ' ${SCHEMA_FILE} | sed -e 's/_//g' -e 's/.* \([[:digit:]]\+\)) do/\1/')
else
  SCHEMA_VERSION=$(sed -n '/COPY "schema_migrations" (version) FROM STDIN/,/\\\./p' ${SCHEMA_FILE} | grep -o "[[:digit:]]*" | awk -v max=0 '{if($1>max){max=$1}}END{print max}')
fi

# Stash DB_SCHEMA_VERSION, so we can check it at exit.
DB_SCHEMA_VERSION="0"

# Compare desired schema version to active schema version in the database
# - set BYPASS_SCHEMA_VERSION to skip version check, and only test DB online
function checkSchemaVersion() {
    # Ask for the current DB schema version, via Rake
    DB_SCHEMA_VERSION=$(/srv/gitlab/bin/bundle exec rake ${SCHEMA_RAKE_TASK})

    # If rake failed, we're not connected to the DB, and DB_SCHEMA_VERSION is empty.
    if [ $? -ne 0 ]; then
      return 1
    fi

    DB_SCHEMA_VERSION=$(echo ${DB_SCHEMA_VERSION} | grep 'Current version: ' | sed -e 's/_//g' -e 's/.* \([[:digit:]]\+\)/\1/')

    # Output the current schema version
    echo "Database Schema - current: ${DB_SCHEMA_VERSION}, codebase: ${SCHEMA_VERSION}"

    # If DB_SCHEMA_VERSION is 0, then the DB has not been initialized. Output message as such.
    if [ "${DB_SCHEMA_VERSION}" == "0" ]; then
      echo "NOTICE: Database has not been initialized yet."
    fi

    # Some uses (migrations) only care if the DB is up
    if [ -n "${BYPASS_SCHEMA_VERSION}" ]; then
      return 0
    fi

    # Compare local to db, pass if local less than or equal to db
    [ $SCHEMA_VERSION -le $DB_SCHEMA_VERSION ];
    return $?
}

for i in $(seq 1 $WAIT_FOR_TIMEOUT); do
  echo "Checking database connection and schema version"
  if checkSchemaVersion ; then
    if [ "$@" ]; then
      exec "$@"
    else
      exit 0
    fi
  fi
  sleep 1
done

# If DB_SCHEMA_VERSION is 0, then the DB has not been initialized.
# Warn that we're restarting the container whilst we wait.
if [ "${DB_SCHEMA_VERSION}" == "0" ]; then
  echo "WARNING: Database has not been initialized yet."
else
  echo "WARNING: Waiting for all services to be operational, and data migrations to complete."
fi

# Output a message as to how to resolve this container failing.
echo "If this container continues to fail / restart, please see:"
echo "  https://docs.gitlab.com/charts/troubleshooting/index.html#application-containers-constantly-initializing"

exit 1
